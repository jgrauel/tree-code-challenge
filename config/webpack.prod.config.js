const webpack = require('webpack');
const merge = require('webpack-merge');
//var path = require("path");
// require('dotenv').config();

const baseConfig = require('./webpack.base.config');
const serverConfig = require('./webpack.server.base.config');

const optimizationConfig = require('./webpack.opt.config');

const CleanWebpackPlugin = require('clean-webpack-plugin');


const productionConfiguration = function (env) {
  const NODE_ENV = env.NODE_ENV ? env.NODE_ENV : 'development';
  return {
    plugins: [
      new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify(NODE_ENV) }),
      new CleanWebpackPlugin(['dist'])
    ]
  };
}

const prodClient = merge.smart(baseConfig, optimizationConfig, productionConfiguration);
const prodServer = merge.smart(serverConfig, optimizationConfig, productionConfiguration);

module.exports = [prodClient, prodServer];