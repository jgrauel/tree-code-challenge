
const path = require("path");
require("babel-polyfill");
const appRootDir = require("app-root-dir").get();
const nodeExternals = require('webpack-node-externals');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  target: "node",
  externals: [nodeExternals()],
  entry: [path.join(appRootDir, "src/server/server.js")], 
  
  output:{
    path: path.join(appRootDir, "/dist/server"),
    publicPath: '/dist/server',
    filename:"index.js",
    library : "api",
    libraryTarget: "commonjs2"
  },
  
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
          test: /\.scss$/,
          loader: 'css-loader/locals'
      },
      {
          test: /\.(ttf|eot|otf|svg|png)$/,
          loader: 'file-loader?emitFile=false'
      },
      {
          test: /\.(woff|woff2)$/,
          loader: 'url-loader?emitFile=false'
      }

    ]
  },
  
  plugins: [
    new CleanWebpackPlugin(['dist'])
  ],
  
  resolve: {
    alias:{
      utils: "./src/server/utils",
      middleware:"./src/server/middleware",
      models:"./src/server/models"
    }
  }
};
