const path = require("path");
const webpack = require('webpack');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
require("babel-polyfill");
const appRootDir = require("app-root-dir").get();

// define the plugins with props to be used in the config below
// NOTE: later can be read from a .env configure to make dynamic


const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/client/index.html",
  filename: "./index.html"
});
const extraTextPlugin = new ExtractTextPlugin("styles.css");


module.exports = {
  entry:["babel-polyfill", path.join(appRootDir ,"src/client/index.js")], 
  output:{
    path: path.join(appRootDir ,"/dist/client"),
    filename:"app.js",
    // for SSR
    //library : "app",
    //libraryTarget: "commonjs2"
  },
  
  devServer: {
    port: 4040,
    open: true,
    proxy: {"/": "http://localhost:5040"}
  },
  
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /(\.scss|\.css)$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [{
                loader:"css-loader",
                  options:{
                   //modules: true,
                   importLoaders: 1,
                   //localIdentName: "[name]_[local]_[hash:base64]",
                   sourceMap: true,
                   minimize: true          
              }},
              "postcss-loader", 
              {
                loader:"sass-loader",
                options: {
                    data: "$env: " + process.env.NODE_ENV + ";$isLocal:"+ (process.env.NODE_ENV !== "production"? "true" : "false") + ";"
                }
              }]
        })
      }
    ]
  },
  
  plugins: [
    htmlPlugin, 
    extraTextPlugin
  ],
  
  resolve: {
    alias:{
      api:"./src/client/api",
      components:"./src/client/components",
      reducers:"./src/client/reducers",
      sagas:"./src/client/sagas",
      styles:"./src/client/styles",
      "~styles":"./src/client/styles"
    }
  }
};
