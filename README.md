# Tree Code Challenge

Repository to hold code for Tree Challenge demonstrating my knowledge of several technologies, including databases, backend design, and UI/UX by creating a live-updating tree view as a web application.
DEMO - [https://codetesttree.herokuapp.com/]

## Requirements
	●  The tree should contain a group of nodes, with a main (root) node that can have any number of ‘factories’. 
	●  These factory nodes can in turn generate a set amount of random numbers (up to 15), represented as child nodes of their respective factories. 
	●  Factories and children should be created through some means of user input (right click, button press, etc) specifying the number of children to generate (up to 15) and the ranges of those children. 
	●  Factories should have an adjustable name assigned to them, be removable, and have an adjustable lower and upper bound for the random number generation. 
	●  You may use any programming languages and front-end design styles of your choosing to create the project. 
	●  All users should see any changes made to the tree immediately across browsers without refreshing or polling. 
	●  The state of the tree should remain persistent; reloading should not undo any state. 
	●  All of a factory’s existing child nodes should be removed upon each new generation. 
	●  Your project should be secure (log-in?), validate inputs, and protect against injections. 
	●  Your project should be hosted on the web using a service such as Amazon AWS or Heroku to run your submission. 
	●  The project should exhibit both a frontend and backend codebase built by you. 
	●  Use a database on your backend, not Firebase. 

## Tech Stack
- Javascript / HTML / CSS / SCSS
- React : construct the client side application
- NodeJs : used to delivery the server side activities
- SocketIO : used to facilitate real time communication for the app to update in multiple browsers
- Babel : transpiler for leverage of ES6
- Webpack : for compiling the code both client and server
- Heroku : web serving, the package json instructs Heroku to compile code before running
- MongoDB : for data collection/storage, node app connects to remote db hosted by MongoDB
- Redux : used as a store fro providing state to React, and sending to Socket.IO
- LocalStorage : the app saves the users options of open and closed tree and branches 

### Data (i keep a similar structure in store)
- collections :
	- tree-config : stores data about tree(s)
		- _id : (ObjectID)
		- title : (string)
		- color : (string)
		- modificationDate : (datetime)
	- tree-branches : stores data about branches of the tree
		- _id : (ObjectID)
		- name : (string)
		- modificationDate : (datetime)
		- children : (array)
		- countTotal : (number)
		- maxValue : (number)
		- minValue : (number)
		- treeID : ObjectID
	
	+ right now the store also has a node for user - which is synced with localstorage
	- user
		- socketID : (string)
		- options : (object)
			- treeOpen : (boolean)
			- branchsOpen : (array)
				[branchID] : (boolean)
		}


## Other Things I'd Like to Add
- add confirmation before delete
- on init read form url to know which tree to pull data from
- give notifications when users make changes to the tree
- add loading animations for init and population processes
- make users only be able to prune if they added 3 branches
- limit the number of prunes a user can do
- allow users to store their name
- pass tree's theme from db collection
- add custom art and icons
- add test suites
- add indication - aniation on new branch additions
- add server side render of the html
- allow an image to be added to a branch
- have random svg associated with leaves generated
- change tree view to be more of a graphic tree
- more to come...

