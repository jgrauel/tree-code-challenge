/*
 * CONNECTION FACTORY - 
 * This module will be the entry for seting up db connections
 * it recevies a callback that gets passed to the async connetion, and called on success/failure
 * right now it is all encompassed in this one file, could separate later if nedded...
*/
// ------------
const MongoClient = require('mongodb').MongoClient;

import co from 'co';

let dbConfig;
let db;

export const makeConnection = () =>  {
    // 
    if(db){
      return co(function*() {
        // already connected
        console.log('--> Already have a connection, returning it');
        return db;
      })
    }
    
    // read values from env
    dbConfig = { 
        "host":process.env.MONGO_HOST,
        "user": process.env.MONGO_USER, 
        "password": process.env.MONGO_PASS,
        "database": process.env.MONGO_NAME,
        "replicaSet" : process.env.MONGO_REPLICA_SET,
        "ssl": process.env.MONGO_SSL,
        "authSource": process.env.MONGO_AUTH_SOURCE,
        "retryWrites": process.env.MONGO_RETRY_WRITES
    };
      
    // @NOTE we need to concatentate the port onto the host here in order to avoid errors from a default port being added automatically

    const creds = (dbConfig.password && dbConfig.user)? `${dbConfig.user}:${dbConfig.password}@` : '';
    
    let params = [], connExtra = '';  
    if(dbConfig.ssl) params.push(`ssl=${dbConfig.ssl}`);
    if(dbConfig.replicaSet) params.push(`replicaSet=${dbConfig.replicaSet}`);
    if(dbConfig.authSource) params.push(`authSource=${dbConfig.authSource}`);
    if(dbConfig.retryWrites) params.push(`retryWrites=${dbConfig.retryWrites}`);
    if(params.length > 0){ 
       connExtra = '?' + params.join('&');
    }
    
    // note this is a little more indepth because of older driver, but newer has a bug
    // create connection URI +srv
    const connPath = `mongodb://${creds}${dbConfig.host}/${dbConfig.database}${connExtra}`;
    console.log('Using MONGO to get data, path:', connPath);
    //
          
      return co(function*() {
        console.log('Attempting to make a new connection ...');
        
        // Default MondoDB driver... had an error with 3, so reverted back to 2.29 -> doesn use { useNewUrlParser: true }
        db = yield MongoClient.connect(connPath);//
        
        console.log('>>> New connection.. ');
        // Close the Mongo connection on Control+C
        process.on('SIGINT', function() {
            db.close(function () {
                  console.log('Mongo default connection disconnected');
                  process.exit(0);
            });
        });
        
        console.log('>>>> Returning it');
        
        return db;  

      }).catch(function(err) {
        //console.log(err.stack);
        return err;
        
      });
 
}

export default db;
