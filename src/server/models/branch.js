/**
 * @name:Branch MODEL
 * @description model that handles branch specifc data
 *  
 * @module Branch
 */

import co from 'co';
import {makeConnection} from "utils/connection";

import _random from "lodash/random";
import _escape from "lodash/escape";
import _isEmpty from "lodash/isEmpty";

const collectionName = 'tree-branches';

const ObjectID = require('mongodb').ObjectID; 

function populateBranchLeaves(count=15, min=1, max=20){
  let i = 0, leaves = [];
  const isUnique = (max-min) > count;
  console.log("[BRANCHMODEL] populateBranchLeaves... isUnique:",isUnique,", count:",count);
  while(i < count){
     const newNum = _random(min,max);
     if(isUnique && leaves.indexOf(newNum) === -1){
      leaves.push(newNum);
       i++;
     }else{
       leaves.push(newNum);
       i++;
     }
  }
  return leaves;
} 
  
const BranchModel = {
  
  /**
   * @name findAll 
   * @description this will return all branches
   *
   * @returns {Object} account data
  */    
  findAll : () => {
    //console.log("[BRANCHMODEL] findAll...");
    let branchList;
    return co(function*() {   
        // Use connect method to connect to the Server
        const db = yield makeConnection();
        // get the collection
        //console.log("[USER] got db connected...");
        const col = db.collection(collectionName);
        //console.log("[USER] got collection...");
        // do the find
        branchList = yield col.find().toArray();
        // though a error if nothing was returned          
        return branchList;
        
    }).catch(function(err) {
        console.log(err.stack);
        throw err;
    });    
  },
  
  /**
   * @name findById 
   * @description this find method looks up a factory by their unique _Id
   * @param {String} id facotry id
   * @returns {Object} factory data
  */  
  findById : (id) => {
    console.log("[BRANCHMODEL] findById...", id);
    let branch;
    return co(function*() {  
          // Use connect method to connect to the Server
          const db = yield makeConnection();
          // get the collection
          //console.log("[USER] got db connected...");
          const col = db.collection(collectName);
          //console.log("[USER] got collection...");
          // do the find
          branch = yield col.findOne({_id:ObjectID(id)});
          // though a error if nothing was returned          
          return branch;

    }).catch(function(err) {
        console.log(err.stack);
        throw err;
    });
  },
    
  /**
   * Create Factory
   * @param {object} data
   */    
  create : (data) => {

    console.log("[BRANCHMODEL] create...", data);
    
    return co(function*() {

      // Use connect method to connect to the Server
      const db = yield makeConnection();
      const col = db.collection(collectionName);
      
      // Set the date here so it is not dependent on the
      // user's machine date.
      data.modifiedDate = new Date().toISOString().slice(0, 19).replace('T', ' ');
      data.name = _escape(data.name); // convert the characters "&", "<", ">", '"', and "'" to their HTML entities.
      
      // set defaults if not passed
      if(!data.countTotal){
        data.countTotal = 15;//set default total to be genereated
      }
      
      if(!data.minValue){
        data.minValue = 1;//set default min the genreated number can be
      } 
      
      if(!data.maxValue){
        data.maxValue = 75;//set default max the genereated number can be
      }   
      
      // generate random children from min to max
      data.children = populateBranchLeaves(data.countTotal, data.minValue, data.maxValue);

      console.log("[BRANCHMODEL] inserting...");
      const result = yield col.insertOne(data);
      const branchID = result.insertedId;
      const branch = yield col.findOne({_id:ObjectID(branchID)});

      return branch;

    }).catch(function(err) {
        console.log(err.stack);
        throw err;
    });
   
  },

  /**
  * Update
  * @param {string} factoryID
  * @param {object} data
  */        
  update: (branchID, data) => {
    
    console.log("[BRANCHMODEL] update...", branchID, data);
    
    return co(function*() {
      // the re-populate uses the update but passes an empty array
       if( _isEmpty(data.children)) {
          // generate random children from min to max
          console.log("[BRANCHMODEL] udating generate children...", data.countTotal);
          data.children = populateBranchLeaves(data.countTotal, data.minValue, data.maxValue);
       }
      
        // Use connect method to connect to the Server
        const db = yield makeConnection();
        // get the collection
        const col = db.collection(collectionName);
        // do the update
        yield col.updateOne({_id:ObjectID(branchID)},{$set:data});
        
        const branch = yield col.findOne({_id:ObjectID(branchID)});
        //console.log("[BRANCHMODEL] update...branch", branch);
        return branch;
      
    }).catch(function(err) {
        //console.log(err.stack);
        throw err;
    });
                  
  },


  /**
  * Delete
  * @param {string} branchID
  */
  delete: function(branchID){
    return co(function*() {

      const db = yield makeConnection();
      // get the collection
      const col = db.collection(collectionName);
      //console.log("[BRANCHMODEL] removing...",branchID);
      
      yield col.deleteOne({_id:ObjectID(branchID)});
      return branchID;

    }).catch(function(err) {
        //console.log(err.stack);
        throw err;
    });
  },
  
  /**
  * DeleteAll
  * @param {array} branchIDs
   * TODO 
  */
  // deleteMany: function(branchIDs){
    
  //   console.log("[BRANCHMODEL] removing factorie: factoryIDs=",branchIDs);
  //   return co(function*() {
  //     const db = yield makeConnection();
  //     // get the collection
  //     const col = db.collection(collectionName);
      
  //     // need to be sure all in the ObjectID format
  //     const removeIDs = branchIDs.map(branchID =>{
  //       return ObjectID(branchID);
  //     })
  //     const result = yield col.deleteMany({_id:{"$in":removeIDs}});
  //     return result;
      
  //   }).catch(function(err) {
  //       console.log(err.stack);
  //       throw err;
  //       //return next(err);// pass it back up
  //   });
  // }  

}
 
export default BranchModel;