/**
 * @name:USER MODEL
 * @description model that handles user data
 *  TODO: use this model later to track if a user is online, and other information
 * @module User
 */
 
 import co from 'co';
 import {makeConnection} from "utils/connection";
 const collectionName = 'tree-users';
 
 const UserModel = {
   /**
     * @name find
     * @description this find method returns  stored for the user matching passed ID
     * @returns {Object} User Data
     * 
    */ 
    
  findByID : () => {
    console.log("[USERMODEL] find...");
    let user;
    return co(function*() {   
        // Use connect method to connect to the Server
        const db = yield makeConnection();
        // get the collection
        const col = db.collection(collectionName);
  
        // do the find
        user = yield col.findOne();
          
        return user;
        
    }).catch(function(err) {
        console.log(err.stack);
        throw err;
    });    
  },
   
  /**
   * CreateUser
   * helper method, not accessible from application
   * @param {object} data
   */    
  create : (data) => {

    console.log("[USERMODEL] create...", data);
    
    return co(function*() {

      // Use connect method to connect to the Server
      const db = yield makeConnection();
      const col = db.collection(collectionName);
      
      // Set the date here so it is not dependent on the
      // user's machine date.
      data.modifiedDate = new Date().toISOString().slice(0, 19).replace('T', ' ');

      const result = yield col.insertOne(data);

      return result;

    
    }).catch(function(err) {
        console.log(err.stack);
        throw err;
        //return next(err);// pass it back up
    });
   
  },

 
}

export default UserModel;