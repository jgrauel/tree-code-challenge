/**
 * @name:TREE MODEL
 * @description model that handles tree-wide data
 *  
 * @module Tree
 */
 
 import co from 'co';
 import {makeConnection} from "utils/connection";
 const collectionName = 'tree-config';
 
 const TreeModel = {
   /**
     * @name find
     * @description this find method returns any configuration stored for the tree
     * @returns {Object} Configuraion Data on the tree
     * 
    */ 
    
  find : () => {
    console.log("[TREEMODEL] find...");
    let tree;
    return co(function*() {   
        // Use connect method to connect to the Server
        const db = yield makeConnection();
        // get the collection
        const col = db.collection(collectionName);
  
        // do the find
        tree = yield col.findOne();
          
        return tree;
        
    }).catch(function(err) {
        console.log(err.stack);
        throw err;
    });    
  },
   
  /**
   * CreateTree
   * helper method, not accessible from application
   * @param {object} data
   */    
  create : (data) => {

    console.log("[TREEMODEL] create...", data);
    
    return co(function*() {

      // Use connect method to connect to the Server
      const db = yield makeConnection();
      const col = db.collection(collectionName);
      
      // Set the date here so it is not dependent on the
      // user's machine date.
      data.modifiedDate = new Date().toISOString().slice(0, 19).replace('T', ' ');

      const result = yield col.insertOne(data);

      return result;

    
    }).catch(function(err) {
        console.log(err.stack);
        throw err;
        //return next(err);// pass it back up
    });
   
  },

 
}

export default TreeModel;