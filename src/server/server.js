
import "babel-polyfill";
require('dotenv').config();

const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');

const appRootDir = require('app-root-dir').get();

const socketIO = require('socket.io');

import co from 'co';
// import _isEmpty from 'lodash/isEmpty';
import {makeConnection} from 'utils/connection';

// middleware
import apiRoutes from 'middleware/api/routes';

// models
import TreeModel from "models/tree";
import BranchModel from "models/branch";
// import UserModel from "models/user";

//
const staticPath = path.resolve(appRootDir, 'dist/client');
const isProd = process.env.NODE_ENV !== 'production'? false : true;

// ------------------------------------------------
// make our express app...
const app = express();

// Don't expose any software information to potential hackers.
app.disable("x-powered-by");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Point static path to dist 
app.use(express.static(staticPath));

// pass down to our configured routes middleware
console.log("Server Starting up...isProd=", isProd);

if(!isProd){
  // helper endpoints 
  app.use('/api', apiRoutes);
}


app.get('/*', (req, res) => {
    const routePath = path.join(appRootDir, 'dist/client/' + 'index.html');
    //console.log('sending file...',routePath);
    res.sendFile(routePath);
})

/** Get port from environment and store in Express. */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Error Catch
 * catch 404 and forward to error handler
 * This is set to catch request for files that dont exist
 * 
 *
 * @param {Object} req request data
 * @param {Object} res response data
 * @param {Object} next allows the middleware to proceed
 * @returns {null} sets an error object and proceeds next
 */  
  app.use(function(req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      
      next(err);
  });

/**
 * Error Handler 
 * @param {Object} req request data
 * @param {Object} res response data
 * @param {Object} next allows the middleware to proceed
 * @returns {} returns an object as a json file with the message
 */
  app.use(function (err, req, res) {
    switch(err.name){
      case "UnauthorizedError":
        return res.status(401).json({"message" : "Unauthorized: " + err.message});
        
      case "NotFound":
        return res.status(404).json({"message" : "Not Found: " + err.message});
    }

    res.status(err.status || 500);
    const result = {
      error :  (!isProd)? err : null,
      message : err.message || 'Oops, Unknown Error'
    }
    // development error handler
    // will print stacktrace
    
    // production error handler
    // no stacktraces leaked to user   
       
    return res.json(result);

  });
// ------------------------------------------------
/** Create HTTP server. */
const server = http.createServer(app);

// create socket using the Server
const io = socketIO(server); 


/**
 * Event listener for HTTP server "error" event.
 */
function onError(error){
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    case 'ETIMEDOUT':
        console.error(bind + ' timed out');
        process.exit(1);
        break;
    default:
      throw error;
  }
}
      
/**
 * Event listener for HTTP server "listening" event.
 * should we wait and connect to db before or after listening ?
 */
function onListening(){
    console.log(`Server Running on port ${port}`);
}
/**
 * Listen on provided port, on all network interfaces.
 * wait to set the listen until we know a db connection is made
 * so the api can return data
 */
server.on('error', onError);
server.on('listening', onListening); 

/**
*  connect to the db
*/ 
co(function*() {
  const db = yield makeConnection();
  //
  //console.log(" connection result");
  if(!db.Error){
    // success so start listening
    console.log('Connection established instructing app to listen on port', port);
    // tell server to start listening
    server.listen(port);        
  }else{
    throw new Error('Unable to connect to database');
  }

}).catch(function(err) {
  
  console.log(err.stack);  
});


/* 
*  Socket 
* NOTE: wouldnt mind have the socket connection path be a sub route																   
*/
const connections = [];

io.on('connection', function (socket) {
	// console.log("Tree App Connected to Socket!!" + socket.id)	
	connections.push(socket)
  
	socket.on('disconnect', () => {
		console.log('Tree Disconnected - '+ socket.id);
    // TODO mark user as logged out
	});
  
  
  // this gets fired after they provide a name
  socket.on('growTree', (userData) => {
    // call endpoint to init Tree
    let treeConfig = {};
    co (function*(){
       
       // TODO: add the user's name and socket.id to users table
       const user = {socketID:socket.id, name:userData && userData.name || 'unknown'};
       
       //
       const tree = yield TreeModel.find(); //
       
       if(!tree){
         throw new Error('Unable to Locate Tree');
       }
       const branchList = yield BranchModel.findAll(); //
       const branches = {};
       branchList.map((branch) =>{
         branches[branch._id] = branch;
       })
      
      treeConfig = {tree, branches, user};
      // console.log('[SOCKET] returning treeConfig ...', treeConfig);
      // return initial tree config - just to this socket
      socket.emit('initTree',treeConfig);
    })
    .catch(function(err) {
      console.log(err.stack);
      socket.emit('initError',err);
    });  
  })
  
  //
	// Set up listeners that match the endpoints of the api 
	socket.on('addBranch', (addData) => {
    // console.log("Tree App Add Branch ",addData)	
    //
    return co (function*(){
      if(!addData){
        throw new Error('No data passed');
      }
      
      const result = yield BranchModel.create(addData);
      // console.log("Branch Added - emitting...");
      // let everyone know a branch was added
      io.emit('addBranchSuccess',result);
      
    }).catch(function(err) {
        console.log(err.stack);
        socket.emit('addBranchFailure',err);
    });
	
	})
  
  socket.on('updateBranch', (branchID, data) => {
    console.log("Tree App Update Branch ", branchID, data);	
    return co (function*(){
      if(!branchID){
        throw new Error('No branchID passed');
      }
      
      const result = yield BranchModel.update(branchID, data);
      // console.log("Branch Updated - emitting...");
      // let everyone know a branch was added
      io.emit('updateBranchSuccess',result);
      
    }).catch(function(err) {
        console.log(err.stack);
        socket.emit('updateBranchFailure',err);
    });
  })

	socket.on('populateBranch',(branchID, data) => {

    console.log("Tree App populateBranch ", branchID);
    return co (function*(){
      if(!branchID){
        throw new Error('No branchID passed');
      }
      // data.children = [];// reset to force new generate -- could do on client side
      const response = yield BranchModel.update(branchID, data);
      // console.log("Branch Populated - emitting...");
      // let everyone know a branch was added
      io.emit('populateBranchSuccess',response);
      
    }).catch(function(err) {
        console.log(err.stack);
        socket.emit('populateBranchFailure',err);
    });
  
	})
	
	socket.on('deleteBranch',(branchID) => {

    console.log("Tree App deleteBranch ", branchID);
    return co (function*(){
      if(!branchID){
        throw new Error('No branchID passed');
      }
      
      const response = yield BranchModel.delete(branchID);
      console.log("Branch delete - emitting...");
      // let everyone know a branch was removed
      io.emit('deleteBranchSuccess', response);
      
    }).catch(function(err) {
        console.log(err.stack);
        socket.emit('deleteBranchFailure',err);
    });
  
  })
  
});


