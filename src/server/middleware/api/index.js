

import TreeModel from "models/tree";
import BranchModel from "models/branch";

import co from 'co';

const ApiMiddleware = {
  /**
    * @name findTree
    * @description use model to get any configuration about the tree
    * @returns {Next} 
    * step one in the application init 
   */ 
  findTree : function(req, res, next){

    return co (function*(){
       // const {user} = req.user;//accountID = '596d16289e6eb00b3af44e51';
       //if(!account){
        // return next(new Error('No accountID provided or located in user'));
       //}
       const treeInfo = yield TreeModel.find(); //
       
       if(!treeInfo){
         return next(new Error('Unable to Locate Tree'));
       }
       req.treeInfo = treeInfo;
       return next();
    
    })
    .catch(function(err) {
        console.log(err.stack);
       return next(err);
    });
      
  },
  
  /**
    * @name buildTree
    * @description use configuration from previous step and adds all the factories
    * @returns {JSONObject} 
    * step two in the application init 
    * return an object with tree configuration and the existing factories
   */
  buildTree : function(req, res, next){
    const config = {};
    const {treeInfo} = req; //will be the init data 
    console.log('[API] buildTree...',treeInfo);
    return co (function*(){
      // get all the factories from the db
       const branches = yield BranchModel.findAll(); //
       
      console.log('[API] got factories returning...');
      return res.json({tree:treeInfo, branches:branches || []});
      
    })
    .catch(function(err) {
        console.log(err.stack);
       return next(err);
    });


  },
  createTree : function(req, res, next){
    return co (function*(){
      const data = req.body
      if(!data){
        throw new Error('No data passed');
      }

      const result = yield TreeModel.create(data);
      const response = {ok:true, _id:result.insertedId}

      return res.json(response);
    }).catch(function(err) {
        console.log(err.stack);
        next(err);
    });
  }, 
   
  listBranches : function(req, res, next){
    const config = {branches:true}; //will be the init data 
    console.log('[API] List Branches...');
    return res.json(config);
  },
  
  createBranch : function(req, res, next){
    console.log('[API] createBranch...');
    return co (function*(){
      const data = req.body
      if(!data){
        throw new Error('No data passed');
      }

      const result = yield BranchModel.create(data);
      const response = {ok:true, _id:result.insertedId}

      return res.json(response);
    }).catch(function(err) {
        console.log(err.stack);
        next(err);
    });
  },
  
  updateBranch : function(req, res, next){
    const config = {branchUpdated:true}; //will be the init data 
    console.log('[API] updateFactory...');
    return res.json(config);
  },
  
  createBranchChildren : function(req, res, next){
    const config = {branchChildren:true}; //will be the init data 
    console.log('[API] createBranchChildren...');
    return res.json(config);
  },
  
  deleteBranch : function(req, res, next){
    const config = {deletedBranch:true}; //will be the init data 
    console.log('[API] deleteBranch...');
    return res.json(config);
  },    
  
}

export default ApiMiddleware;