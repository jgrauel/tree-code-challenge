/**
 * @name API Routes 
 * set endpoints for api
*/
// import path from "path";
import express from "express";
import api from "middleware/api";
const isProd = process.env.NODE_ENV !== 'production'? false : true;
  /*
  –	[get] listFactories
  –	[get] factory (single factory by id)
  –	[get] factory children - this could be same as generate? 
  –	[post] factory (create new factory)
  –	[put] factory (set factory name, label, status, icon) name, iconName, status
  –	[put]  generate factory children and store in factory , number, rangeFrom, rangeTo
  –	[delete] delete factory
  –	[put] tree (set/update your tree name)
  */
const apiRoutes = express.Router();

  apiRoutes.get("/tree", api.findTree, api.buildTree);
  if(!isProd){
    apiRoutes.post("/tree", api.createTree);
  }
  
  
  apiRoutes.get("/branches", api.listBranches);
  apiRoutes.get("/branches/:branchID", api.listBranches);
  
  apiRoutes.post("/branches", api.createBranch); 
  apiRoutes.put("/branches/:branchID", api.updateBranch);
  apiRoutes.put("/branches/:branchID/generate", api.createBranchChildren);

  apiRoutes.delete("/branches/:branchID", api.deleteBranch);

  // catch
  apiRoutes.use('*', (req, res) => {
    return res.send("nothing here");
  });

export default apiRoutes;