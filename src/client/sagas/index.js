import { takeEvery, put, call, select } from 'redux-saga/effects';

import * as CONST from 'actions/constants';
import {saveState} from 'api/localStorage';
import { fromJS, toJS } from "immutable";

/**
 *  SAGA WATCHERS
 * this is the overall saga that sets up watchers 
 * to listen when to run other sub sagas
 * 
 */
export default function* rootSaga() {
  yield takeEvery(CONST.APP_INIT_REQUEST, initAppSaga);
  yield takeEvery(CONST.BRANCH_ADD_REQUEST, addBranchSaga);
  yield takeEvery(CONST.BRANCH_UPDATE_REQUEST, updateBranchSaga);
  yield takeEvery(CONST.BRANCH_POPULATE_REQUEST, populateBranchSaga);
  yield takeEvery(CONST.BRANCH_DELETE_REQUEST, deleteBranchSaga);
  yield takeEvery(CONST.TOGGLE_BRANCH_REQUEST, branchToggleSaga);
  yield takeEvery(CONST.TOGGLE_TREE_REQUEST, treeToggleSaga);
}

// TODO add a event channel to listen for socket emits from the server


// Selectors
export const getUser = (state) => {
  console.log("[SELECTORS] getUser ",);
  return state.getIn(["data", "user"]).toJS();
};

export function* initAppSaga(action) {
  const { socket } = action;
  // make sure data is safe...
  // not getting here with an object, 
  try{
    console.log("[SAGAS] initAppSaga");
    socket.emit('growTree',{}); 

  } catch (error) {
    const { message } = error;
    console.log("[SAGAS] initAppSaga- error:", error);
    yield put({ type: CONST.APP_INIT_FAILURE, message });
  }
  
}

/**
 * Called when user clicks to toggle tree
 * we store in their user info and in local storage - so pref will remain
 * @param {objec} action 
 */
export function* treeToggleSaga(action){
  // save user options to local storage
  try{
    const user = yield select(getUser);
    console.log("[SAGAS] treeToggleSaga- action", action, user);
    yield call(saveState,{data:{user:{options:user.options}}} );
  } catch (error){
    console.log("[SAGAS] treeToggleSaga- ERROR", error);
  }

}

/**
 * Called when user clicks to toggle tree
 * we store in their user info and in local storage - so pref will remain
 * @param {objec} action 
 */
export function* branchToggleSaga(action){
  // save user options to local storage 
  try{
    const user = yield select(getUser);
    console.log("[SAGAS] branchToggleSaga- action", action, user);
    yield call(saveState,{data:{user:{options:user.options}}} );
  } catch (error){
    console.log("[SAGAS] branchToggleSaga- ERROR", error);
  }
  

}

//------------------------------
// BRANCH CONTROL
export function* addBranchSaga(action) {
    const { socket, data } = action;
    // make sure data is safe...
    // not getting here with an object, 
    try{
      console.log("[SAGAS] addBanchSaga- branchData:", data);
      socket.emit('addBranch', data); 
 
    } catch (error) {
      const { message } = error;
      console.log("[SAGAS] addBanchSaga- error:", error);
      yield put({ type: CONST.BRANCH_ADD_FAILURE, message });
    }
    
}

export function* updateBranchSaga(action) {
    const { socket, branchID, data } = action;
    // make sure data is safe...
    // not getting here with an object, 
    try{
      console.log("[SAGAS] updateBranchSaga, data:", data);
      socket.emit('updateBranch', branchID, data); 
    
    } catch (error) {
      console.log("[SAGAS] updateBranchSaga- error:", error);
      const { message } = error;
      yield put({ type: CONST.BRANCH_UPDATE_FAILURE, message });
    }  
}

export function* populateBranchSaga(action) {
    const { socket, branchID, data } = action;
    // make sure data is safe...
    // not getting here with an object, 
    try{
      //data._id = null; // make sure we dont pass the id in data object
      //data.children = []; //we want to force a reset..
      console.log("[SAGAS] populateBranchSaga, data:", data);
      socket.emit('populateBranch', branchID, data); 
    
    } catch (error) {
      const { message } = error;
      console.log("[SAGAS] populateBranchSaga- error:", error);
      yield put({ type: CONST.BRANCH_POPULATE_FAILURE, message });
    }  
}

export function* deleteBranchSaga(action) {
  const { socket, branchID } = action;
  // make sure data is safe...
  // not getting here with an object, 
  try{
    console.log("[SAGAS] deleteBranchSaga :", branchID);
    socket.emit('deleteBranch', branchID); 
  
  } catch (error) {
    const { message } = error;
    console.log("[SAGAS] deleteBranchSaga- error:", error);
    yield put({ type: CONST.BRANCH_DELETE_FAILURE, message });
  }  
}

