import * as CONST from "./constants";

export const setInitRequest = (socket) => {
  //console.log("[ACTION] setInitRequest ", projectId);
  return {
    type: CONST.APP_INIT_REQUEST,
    socket
  };
};

export const setInitSuccess = response => {
  //console.log("[ACTION] setInitSuccess ", response);
  return {
    type: CONST.APP_INIT_SUCCESS,
    response
  };
};

export const setInitFailure = message => {
  //console.log("[ACTION] setInitFailure ", message);
  return {
    type: CONST.APP_INIT_FAILURE,
    message
  };
};

// USER INTERFACE
export const openTreeToggle = (toggle) =>{
  return {
    type: CONST.TOGGLE_TREE_REQUEST,
    toggle
  };
}

export const openBranchToggle = (toggle, branchID) =>{
  return {
    type: CONST.TOGGLE_BRANCH_REQUEST,
    toggle,
    branchID
  };
}

// BRANCH ACTIONS
export const addNewBranch = (socket, data) =>{
  return {
    type: CONST.BRANCH_ADD_REQUEST,
    socket,
    data
  };
}

export const addBranchSuccess = response =>{
  return {
    type: CONST.BRANCH_ADD_SUCCESS,
    branch : response
  };
}

export const addBranchFailure = message => {
  return {
    type: CONST.BRANCH_ADD_FAILURE,
    message
  };
}

// I know update and populate really are the same- but started with them sep - 
// come back later and optimize
export const updateBranch = (socket, branchID, data) =>{
  return {
    type: CONST.BRANCH_UPDATE_REQUEST,
    socket,
    branchID,
    data
  };
}

export const updateBranchSuccess = response =>{
  return {
    type: CONST.BRANCH_UPDATE_SUCCESS,
    branch : response
  };
}

export const updateBranchFailure = message => {
  return {
    type: CONST.BRANCH_UPDATE_FAILURE,
    message
  };
}

export const populateBranch = (socket, branchID, data) =>{
  return {
    type: CONST.BRANCH_POPULATE_REQUEST,
    socket,
    branchID,
    data
  };
}

export const populateBranchSuccess = response =>{
  return {
    type: CONST.BRANCH_POPULATE_SUCCESS,
    branch : response
  };
}

export const populateBranchFailure = message => {
  return {
    type: CONST.BRANCH_POPULATE_FAILURE,
    message
  };
}

export const deleteBranch = (socket, branchID, data) =>{
  return {
    type: CONST.BRANCH_DELETE_REQUEST,
    socket,
    branchID,
    data
  };
}

export const deleteBranchSuccess = response =>{
  return {
    type: CONST.BRANCH_DELETE_SUCCESS,
    branchID : response
  };
}

export const deleteBranchFailure = message => {
  return {
    type: CONST.BRANCH_DELETE_FAILURE,
    message
  };
}