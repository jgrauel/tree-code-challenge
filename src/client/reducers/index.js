
import {fromJS} from 'immutable';
import {combineReducers} from 'redux-immutable';
import * as CONST from 'actions/constants';


import data from 'reducers/data';
/** **************************************
 * BASE REDUCER
 ****************************************/
export const def = {
  isInit: false,
  isFetching: false
};

// values passed from the router headers
const base = (state = fromJS(def), action) => {
  const { message } = action; // object wth name and message
  let errorMessage;
  //console.log("[REDUCERS] BASE", action);
  switch (action.type) {
      
    case CONST.APP_INIT_REQUEST:
      return state.set("isFetching", true); //
  
    case CONST.APP_INIT_SUCCESS:
      return state
        .set("isFetching", false)
        .set("isInit", true)
        .delete("error");

    case CONST.APP_INIT_FAILURE:
      errorMessage = `${message.name || 'Error'}: ${message.message || 'oops :{'}`;
      return state
        .set("isFetching", false)
        .set("isInit", false)
        .set("error", errorMessage);
        
    case CONST.BRANCH_ADD_REQUEST:
      return state
        .set("isSaving", true);

    case CONST.BRANCH_ADD_SUCCESS:
    case CONST.BRANCH_UPDATE_SUCCESS:
    case CONST.BRANCH_POPULATE_SUCCESS:
      return state
        // .set("notification") //TODO provide feedback
        .set("isSaving", false); 

    case CONST.BRANCH_ADD_FAILURE:
      errorMessage = `${message.name || 'Error'}: ${message.message || 'oops :{'}`;
          return state
            .set("isSaving", false)
            .set("error", messaerrorMessagege);
    
    case CONST.BRANCH_UPDATE_FAILURE:
    case CONST.BRANCH_POPULATE_FAILURE:
      errorMessage = `${message.name || 'Error'}: ${message.message || 'oops :{'}`;
            return state
              .set("error", errorMessage);
    default:
      return state;
  }
};


const treeApp = combineReducers({
  base,
  data
});

export default treeApp;
