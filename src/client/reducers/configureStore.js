import { createStore, applyMiddleware } from "redux";

import treeApp from "reducers";
import createSagaMiddleware, { END } from "redux-saga";
import rootSaga from "sagas";
import { fromJS } from "immutable";
//import {loadState} from 'api/localStorage';

import { composeWithDevTools } from "redux-devtools-extension";
// setting up options to limit use on production of the extension
const composeOptions = {
  shouldRecordChanges: process.env.NODE_ENV !== "production",
  autoPause: process.env.NODE_ENV === "production",
  features: {
    export: process.env.NODE_ENV !== "production",
    import: process.env.NODE_ENV !== "production",
    jump: process.env.NODE_ENV !== "production",
    skip: process.env.NODE_ENV !== "production",
    reorder: process.env.NODE_ENV !== "production",
    dispatch: process.env.NODE_ENV !== "production",
    test: process.env.NODE_ENV !== "production"
  }
};

const composeEnhancers = composeWithDevTools(composeOptions);

const configureStore = initialState  => {
  // console.log("[CONFIGURESTORE] initialState:", initialState);
  //initialState = fromJS(loadState()) || null; // get user fomr local store
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware]; //
  const enhancer = composeEnhancers(
    applyMiddleware(...middlewares) //addding enhancemtns
    // other store enhancers if any
  );

  const store = initialState
    ? createStore(treeApp, fromJS(initialState), enhancer)
    : createStore(treeApp, fromJS({}), enhancer);

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);
  sagaMiddleware.run(rootSaga);
  return store;
};

export default configureStore;
