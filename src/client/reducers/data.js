import { fromJS } from "immutable";
import { combineReducers } from "redux-immutable";
import * as CONST from "actions/constants";

const tree = (state = fromJS({}), action) => {
  //console.log("[REDUCERS] DATA-TREE", action);
  const { response } = action;
  switch (action.type) {
    case CONST.APP_INIT_SUCCESS:
      return state.merge(fromJS(response.tree)); 
    default:
      return state;
  }
}

const branches = (state = fromJS({}), action) => {
   console.log("[REDUCERS] DATA-BRANCHES action:", action);

  const { response, branch} = action;
  let {branchID} = action;
  let updatedBranch;
  if( branch && branch._id ) {
    branchID = branch._id;
  }
  switch (action.type) {
    case CONST.APP_INIT_SUCCESS:
      return state.merge(fromJS(response.branches));
      
    case CONST.BRANCH_ADD_SUCCESS:
      console.log("[REDUCERS] DATA-BRANCHES", branchID);
      //branchID = branch._id;
      return state.merge(fromJS({ [branchID]: { ...branch } }));
    
    case CONST.BRANCH_UPDATE_REQUEST:
      return state.setIn([branchID,'isUpating'], true); 

    case CONST.BRANCH_UPDATE_SUCCESS:
      // console.log("[REDUCERS] DATA-BRANCHES UPDATE", branchID);
      updatedBranch = state
        .get(branchID) //
        .merge(fromJS(branch))
        .set('isUpating', false);

      return state
        .set(branchID, updatedBranch);
        
    
    case CONST.BRANCH_POPULATE_REQUEST :
      return state.setIn([branchID,'isPopulating'], true);

    case CONST.BRANCH_POPULATE_SUCCESS:
      // console.log("[REDUCERS] DATA-BRANCHES", branchID);
      updatedBranch = state
          .get(branchID)
          .merge(fromJS(branch))
          .set('isPopulating', false);

        return state
          .set(branchID, updatedBranch);

    case CONST.BRANCH_DELETE_SUCCESS:
      console.log("[REDUCERS] DATA-BRANCHES", branchID);
      return state.delete(branchID);
    
      default:
      return state;
  }
}

const defUser = {
  socketID : '',
  options : {
    treeOpen : true,
    branchesOpen : {},
  }
}

const user = (state = fromJS(defUser), action) => {
  console.log("[REDUCERS] DATA-USER", action);
  const { response , toggle, branchID } = action;
  switch (action.type) {
    case CONST.APP_INIT_SUCCESS:
      return state.merge(fromJS(response.user)); 
    case CONST.TOGGLE_BRANCH_REQUEST :
      return state.setIn(['options','branchesOpen', branchID ], toggle);
    case CONST.TOGGLE_TREE_REQUEST : 
      return state.setIn(['options','treeOpen'], toggle);
    default:
      return state;
  }
}
const data = combineReducers({
  tree,
  user,
  branches
});

export default data;
