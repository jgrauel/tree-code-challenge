
import {Map, fromJS} from "immutable";
import _orderBy from "lodash/orderBy";

export const getAppProps = (state, ownProps) => {
  const base = state.get("base", {}).toJS(); 
  const data = state.get("data", {}).toJS();

  const {
    isInit,
    isFetching,
    error
  } = base; //
  const { tree, branches, user } = data || {};

  //console.log("[MAPTOPROPS] getAppProps ");
  // order branches newest to oldest -- this converts to an array
  const branchesOrdered = _orderBy(branches, ['modifiedDate'],['desc'])
  // which will display the ones marked complete, and then call a remove
  // we need to pass the stories from data so the menu can be built
  return {
    isInit,
    isFetching,
    error,
    tree,
    user,
    branches : branchesOrdered
  };
};