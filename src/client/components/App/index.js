
import React,{Component} from 'react';
import PropTypes from 'prop-types';
import io from 'socket.io-client';
import { connect } from 'react-redux'; // handles the state store

import * as baseActions from 'actions';
import {getAppProps} from 'reducers/mapToProps';

import Branches from 'components/Branches';
import BranchForm from 'components/BranchForm';


import Button from '@material-ui/core/Button';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import AddIcon from '@material-ui/icons/Add';
import Divider from '@material-ui/core/Divider';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CssBaseline from '@material-ui/core/CssBaseline';


import _size from 'lodash/size';
import _isEmpty from 'lodash/isEmpty';

import './App.scss';
// server path - TODO read from the .env
const socketPath = process.env.SOCKET_URL;

const socket = io();

const theme = createMuiTheme({
  palette: {
    primary: {
      main: 'rgba(87, 155, 251, 1)',
    },
    secondary: {
      main: '#ffab00',
    },
    tertiary: {
      main: '#1e67a3',
    },
  },
})

const Overview = (props) => {
  const {tree, user} = props;
  return (
    <div className="overview">
      <h1>Welcome User {user.socketID} to my code challenge!</h1>
      <h3>{tree.title}</h3>
        <ul>
          <label>The goal of the challege was to :</label>
        <li>The <em>tree</em> should contain a group of nodes, with a main (root) node that can have any number of 'branches'.</li>
        <li>The branch nodes should generate a set amount of random numbers (up to 15), represented as child nodes of their respective branches.</li> 
        <li>Branches and children should be created through some means of user input (right click, button press, etc) specifying the number of children to generate (up to 15) and the ranges of those children.</li>  
        <li>Branches should have an adjustable name assigned to them, be removable, and have an adjustable lower and upper bound for the random number generation.</li>  
        <li>I was allowed to use any programming languages and front-end design styles of my choosing.</li>  
        <li>All users should see any changes made to the tree immediately across browsers without refreshing or polling.(Open a new broswer window to test this) </li>
        <li>The state of the tree should remain persistent; reloading should not undo any state. </li> 
        <li>All of a branches's existing child nodes should be removed upon each new generation.</li>  
        <li>The project should be secure validate inputs, and protect against injections.</li>  
        <li>The project should be hosted on the web using a service such as Amazon AWS or Heroku to run your submission.</li> 
        <li>The project should exhibit both a frontend and backend codebase built by me. </li> 
        <li>Use a database on your backend, not Firebase.</li>  
      </ul>
      
      <p>So here it is. Of course its a work in progress and not a production ready application.</p>
      <p>Poke around, add a branch, rename it, generatate a few children and let me know your thoughts.</p>
    </div>
  );
}

class App extends Component{
  constructor(props){
    super(props);
    // console.log("[APP] socketPath=", socketPath);
    const {user} = props;
    const {options} = user || {};
    this.state = {
      activeBranch : null,
      editorOpen : false,
      expanded : options.treeOpen || true // start open
    }
    // set up the socket events and connect to dispatchings
    // when we fist connect we can show the init...
    //props.setInitRequest(socket);
    
    // -----------------------
    // Socket Related
    // TODO: to move these out into a saga for listening...
    // capture  socket emitters...
    socket.on('initTree', (config) => {
        // dispatch event to store
        props.setInitSuccess(config);
    });
    
    socket.on('initError', (error) => {
        // dispatch event to store
        props.setInitFailure(error);
    });
    
    socket.on('addBranchSuccess', (branch) => {
        console.log("[SOCKETEVENT] branchAdded");
        props.addBranchSuccess(branch);
    }); 

    socket.on('addBranchFailure', (error) => {
        console.log("[SOCKETEVENT] fail");
        props.addBranchFailure(error);
    });
    
    //
    socket.on('updateBranchSuccess', (branch) => {
        console.log("[SOCKETEVENT] branchUpdated");
        props.updateBranchSuccess(branch);
    }); 
    
    socket.on('updateBranchFailure', (error) => {
        console.log("[SOCKETEVENT] fail");
        props.updateBranchFailure(error);
    });
    
    //
    socket.on('populateBranchSuccess', (branch) => {
        console.log("[SOCKETEVENT] branchPopulated>>>");
        props.populateBranchSuccess(branch);
    }); 
    
    socket.on('populateBranchFailure', (error) => {
        console.log("[SOCKETEVENT] fail");
        props.populateBranchFailure(error);
    });

       //
    socket.on('deleteBranchSuccess', (branchID) => {
        console.log("[SOCKETEVENT] branchRemove>>>");
        props.deleteBranchSuccess(branchID);
    }); 
    
    socket.on('deleteBranchFailure', (error) => {
        console.log("[SOCKETEVENT] fail");
        props.deleteBranchFailure(error);
    });
    // -----------------------
  
  }
  
  // NOTE: put this in a method, incase later wanted to wait 
  // and start until login or some other user interaction
  // TODO move to a saga....
  // getStartedHandler = () => {
  //   //console.log("[APP] getStartedHandler", this.props);
  //   socket.emit('growTree', {}); 
  // }
    
  openBranchEditor = (branch) => () => {
    // console.log("[APP] openBranchEditor Modal", branch);
    this.setState({ activeBranch: branch, editorOpen: true });
  }
  
  closeBranchEditor = () => () => {
    console.log("[APP] closeBranchEditor -->");
    this.setState({ activeBranch: null,  editorOpen:false});
  }

  handleTreeDisplay = () => (event, expanded) =>{
    // this.setState(
    //  { expanded: expanded ? true : false}
    // )
    this.props.openTreeToggle(expanded);
  }
  
  render(){
    const {isInit , isFetching, error, 
      tree, branches, user, 
      addNewBranch, updateBranch, populateBranch, deleteBranch, 
      openBranchToggle} = this.props;
    const {editorOpen, activeBranch} = this.state;
    const {options} = user || {};
    const expanded = options.treeOpen;

    const title = tree && tree.title || "";
    const notLoaded = error? <h1>{error.message || 'oops :('}!</h1> : <div className="loading">planting a seed...</div>;

    return (
      <React.Fragment>
      <CssBaseline />
      <MuiThemeProvider theme={theme}>
      <main className="layout">
        {(isInit && !isFetching) ? 
          <AppBar color="primary">
            <h4>User : {user.name || "???"} - {user.socketID}</h4> 
          </AppBar>  : null}
          
      { (isInit && !isFetching)?  
        <div className="tree-container">
          <ExpansionPanel expanded={expanded} onChange={this.handleTreeDisplay()}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
              <h2>{title} ({_size(branches)})</h2>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className="tree-contents">
              <ExpansionPanelActions className="tree-actions">
                <Button variant="contained" aria-label="New Branch" size="large" color="tertiary" onClick={this.openBranchEditor()}>
                  <AddIcon />Add Branch 
                </Button>
              </ExpansionPanelActions>                
              <Branches branches={branches} userOptions={options}
                openBranchToggle={openBranchToggle}
                openBranchEditor={this.openBranchEditor} 
                closeBranchEditor={this.closeBranchEditor()}
                updateBranch={updateBranch}
                populateBranch={populateBranch} 
                deleteBranch={deleteBranch} />            
            </ExpansionPanelDetails> 
 
          </ExpansionPanel>
        </div>    

      :
        notLoaded
      }
      <aside className="gutter">
      {isInit && !isFetching && editorOpen? 
        <BranchForm 
            formType={!_isEmpty(activeBranch)?'update':'add'}
            user={user}
            branch={activeBranch} 
            updateBranch={updateBranch} 
            addNewBranch={addNewBranch}
            handleClose={this.closeBranchEditor()} />          
        : null}
      {isInit && !isFetching && !editorOpen? 
        <Overview tree={tree} user={user} /> : null }
      </aside>
      </main> 
      </MuiThemeProvider>
      </React.Fragment>
    );
  }

  componentDidMount() {
    // console.log("[APP] mounted");
    this.props.setInitRequest();
  }  
  componentWillUnmount() {
    socket.disconnect()
    // alert("Disconnecting Socket as component will unmount")
  }
  
};

App.propTypes = {
  user: PropTypes.object,
  tree: PropTypes.object,
  branches: PropTypes.array,
  isInit: PropTypes.bool,
  isFetching: PropTypes.bool,
  error: PropTypes.string,
  notification : PropTypes.string,
};

const mapStateToProps = (state, ownProps) => getAppProps(state, ownProps);
function mergeProps(stateProps, dispatchProps) {

  return Object.assign({}, stateProps, {
    // this will allow us to dispatch an action to the store
    // NOTE: put the init in a saga, to allow for waiting 
    // and start until login or some other user interaction
    setInitRequest: () => dispatchProps.setInitRequest(socket),
    setInitSuccess: response => dispatchProps.setInitSuccess(response),
    setInitFailure: response => dispatchProps.setInitFailure(response),
    
    openTreeToggle : toggle => dispatchProps.openTreeToggle(toggle),
    openBranchToggle : (toggle, branchID) => dispatchProps.openBranchToggle(toggle, branchID),
    
    addNewBranch : data => dispatchProps.addNewBranch(socket, data),
    addBranchSuccess: response => dispatchProps.addBranchSuccess(response),
    addBranchFailure: response => dispatchProps.addBranchFailure(response),
    
    updateBranch : (branchID, data) => dispatchProps.updateBranch(socket, branchID, data),
    updateBranchSuccess: response => dispatchProps.updateBranchSuccess(response),
    updateBranchFailure: response => dispatchProps.updateBranchFailure(response),
    
    populateBranch : (branchID, data) => dispatchProps.populateBranch(socket, branchID, data),
    populateBranchSuccess: response => dispatchProps.populateBranchSuccess(response),
    populateBranchFailure: response => dispatchProps.populateBranchFailure(response),

    deleteBranch : (branchID) => dispatchProps.deleteBranch(socket, branchID),
    deleteBranchSuccess: response => dispatchProps.deleteBranchSuccess(response),
    deleteBranchFailure: response => dispatchProps.deleteBranchFailure(response)
    
  });
}

export default connect(mapStateToProps, baseActions, mergeProps)(App);