/**
 * Component to represent each brnach added to the tree
*/
import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';

import _isEmpty from 'lodash/isEmpty';

import './BranchForm.scss';


class BranchForm extends Component{
  constructor(props){
    super(props);
  
    const {branch} = this.props;
    this.state = {
      name : branch && branch.name || '',
      countTotal : branch && branch.countTotal || 15,
      maxValue : branch && branch.maxValue || 25, // note need to be sure that it is at least +1 to countTotal
      minValue : branch && branch.minValue || 1// >=0  
    }
    
    //this.submitForm = this.submitForm.bind(this);
    //this.handleInputChange = this.handleInputChange.bind(this);
  }
  
  submitForm = () => (event) => {
    event.preventDefault();
    //event.stopPropagation();
    const {branch, addNewBranch, updateBranch, handleClose, formType} = this.props;
    console.log("[BRANCHFORM] submit", this.state, ', formType:', formType);
    
    if(_isEmpty(this.state.name)) return;
    // prep the values to remain numbers...the input changes to string
    const postData = {...this.state};
    postData.countTotal = parseInt(postData.countTotal);
    postData.maxValue = parseInt(postData.maxValue);
    postData.minValue = parseInt(postData.minValue);
    
    if(formType === 'add'){
      addNewBranch(postData);
    }else{
      updateBranch(branch._id, postData);
    }
    
    handleClose();
    return false;
  }
  
  handleInputChange = (item) => (event) => {
    let newValue = event.target.value;
    //console.log("[BRANCHFORM] handleInputChange", item, newValue);

    this.setState({
      [item]: newValue
    });    
  }
  
  
  render(){
    console.log("[BRANCHFORM]", this.props, ", state:", this.state);
    const { handleClose, formType} = this.props;
    let {name, countTotal, maxValue, minValue} = this.state;
    let formTitle = `${formType==='add'? 'New' : 'Update'} Branch`;
    let minMax = parseInt(maxValue) - 1;
    return (
      <div className="branch-form">
        <form autoComplete="off" onSubmit={this.submitForm()}>
          <h2>{formTitle}</h2>
          <ul>
            <li>
              <TextField
                required
                autoFocus
                fullWidth
                error={_isEmpty(name)}
                id="name"
                label="Branch Name"
                value={name}
                onChange={this.handleInputChange('name','number')}
                margin="normal"
              />
            </li>
            <li>
              <TextField
                id="countTotal"
                label="Total Leaves"
                value={countTotal}
                onChange={this.handleInputChange('countTotal')}
                type="number"
                InputLabelProps={{
                  shrink: true
                }}
                InputProps={{inputProps : { max: 15, min:1}}}
                helperText="No more than 15 or less than 1!"
                margin="normal"
              /> 
            </li>
            <li>
              <TextField
                id="maxValue"
                label="Maximum number value"
                value={maxValue}
                onChange={this.handleInputChange('maxValue')}
                type="number"
            
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
                helperText={`keep each leaf value unique by setting above total(${countTotal})`}
                InputProps={{inputProps : { min:0}}}
              />
            </li>
            <li>
              <TextField
                id="minValue"
                label="Minimum number value"
                value={minValue}
                onChange={this.handleInputChange('minValue')}
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
                InputProps={{inputProps : { min:1, max:{minMax}}}}
                helperText="Has to be less than max value"
              /> 
            </li>
            <li>
              <Divider/>
              <div className="form-actions">
                <Button id="submit" variant="contained" color="primary" type="submit" 
                  onSubmit={this.submitForm()} onClick={this.submitForm()}>
                    submit
                </Button>
                <Button id="cancel" color="secondary" onClick={handleClose}>
                    cancel
                </Button>
              </div>         
            </li>
          </ul>
        </form> 
      </div>
    );
  }
  
  
};

BranchForm.propTypes = {
  user: PropTypes.object,
  branch: PropTypes.object,
  formType: PropTypes.string,

  updateBranch: PropTypes.func, 
  addNewBranch: PropTypes.func,
  handleClose: PropTypes.func
};

export default BranchForm;