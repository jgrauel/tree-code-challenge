import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Branch from 'components/Branch';

import './Branches.scss';

class Branches extends Component{
  constructor(props){
    super(props);

  }
  
  
  render(){
    const {branches, userOptions,
      populateBranch, deleteBranch, 
      openBranchToggle, openBranchEditor, closeBranchEditor
    } = this.props;

  
    
    return (
      <section className="branches">
      
        {branches && branches.map((branch, index) => {
          return <Branch key={index} branch={branch} 
            userOptions={userOptions}
            openBranchToggle={openBranchToggle}
            populateBranch={populateBranch} 
            openBranchEditor={openBranchEditor}
            closeBranchEditor={closeBranchEditor}
            deleteBranch={deleteBranch}/>
        })}
      
      </section> 
    );
  }
  
  
}

Branches.propTypes = {
  branches: PropTypes.array,
  populateBranch: PropTypes.func, 
  openBranchEditor: PropTypes.func,
  deleteBranch: PropTypes.func,
};

export default Branches;