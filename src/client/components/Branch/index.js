/**
 * Component to represent each brnach added to the tree
*/
import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import Popover from '@material-ui/core/Popover';
import TextField from '@material-ui/core/TextField';

import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import FaceIcon from '@material-ui/icons/Face';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';


import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';

import './Branch.scss';


class Branch extends Component{
  constructor(props){
    super(props);
    
    const {branch} = this.props;
    this.state = {
      popoverEl : null, // remove
      anchorEl : null,
      countTotal : branch && branch.countTotal || 15,
      maxValue : branch && branch.maxValue || 25, // note need to be sure that it is at least +1 to countTotal
      minValue : branch && branch.minValue || 1, // >=0 
      //expanded : false // TODO set this from redux store or local store
    }
  }

  handleBranchDisplay = () => (event, expanded) =>{
    // this.setState(
    //  { expanded: expanded ? true : false}
    // )
    
    console.log("[BRANCHFORM] handleBranchDisplay", expanded);

    if(!expanded){
      this.props.closeBranchEditor();
    }
    this.props.openBranchToggle(expanded, this.props.branch._id);

  }

  showPopulateOptions = () => (event) =>{
    this.setState({anchorEl: event.currentTarget})
  }
  
  hidePopulateOptions = () => (event) =>{
    this.setState({anchorEl: null})
  }

  handleInputChange = (item) => (event) => {
    let newValue = event.target.value;
    //console.log("[BRANCHFORM] handleInputChange", item, newValue);

    this.setState({
      [item]: newValue
    });    
  }

  populateBranch = () => (event) =>{
    event.preventDefault();
    // console.log("[BRANCH] populateBranch", this.props.branch._id, this.popoverEl);
    // update the populate values to reflect changes
    const {branch} = this.props;
    const {countTotal, maxValue, minValue} = this.state;
    const postData = {...branch};
    postData.countTotal = parseInt(countTotal);
    postData.maxValue = parseInt(maxValue);
    postData.minValue = parseInt(minValue);
    postData.children = []; // force a re-generate
    delete postData._id; // can't pass id in the data to be updated
    
    this.props.populateBranch(branch._id, postData); 
    // close the popover
    // this.hidePopulateOptions();
    this.setState({anchorEl: null});

  }

  deleteBranch = () => (event) =>{
    // TODO: add confirmation
    event.preventDefault();
    console.log("[BRANCH] deleteBranch", this.props.branch._id);
    const {branch} = this.props;
   
    this.props.deleteBranch(branch._id); 
  }
  
  render(){
    const {branch, openBranchEditor, userOptions} = this.props;
    const {children, name, _id} = branch; //
    const {anchorEl, countTotal, maxValue, minValue } = this.state;
    const { branchesOpen } = userOptions;
    const expanded = branchesOpen ? branchesOpen[_id] : false;
    let minMax = parseInt(maxValue) - 1;
    //console.log("[BRANCH] render - branch:", branch);
    return (
      <div className={'branch'} >
      <ExpansionPanel expanded={expanded} onChange={this.handleBranchDisplay()}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <h3 className={"branch-heading"}>{name}</h3>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <ul className="leaves">
            {children && children.map((leaf, index) => {
              return <li key={index} className="leaf"><Chip avatar={<Avatar><FaceIcon /></Avatar>} label={`${leaf}`} /></li>
            })}
            </ul>
      </ExpansionPanelDetails>
      <Divider />
      <ExpansionPanelActions>
        <Button size="small" variant="contained" color="secondary" onClick={this.showPopulateOptions()}>
          Re-Populate
        </Button>
        
            <IconButton size="small" variant="contained" color="primary" aria-label="Edit"  onClick={openBranchEditor(branch)}>
              <EditIcon/>
            </IconButton>

            <IconButton size="small" aria-label="Prune" onClick={this.deleteBranch()} >
              <DeleteIcon/>
            </IconButton>
          
      </ExpansionPanelActions>
      <Popover
          ref={(comp) => (this.popoverEl = comp)}
          open={Boolean(anchorEl)}
          anchorEl={anchorEl}
          onClose={this.hidePopulateOptions()}
        >
          <form>
          <MenuList >  
            <MenuItem>
              <TextField
                id="countTotal"
                label="Total Leaves"
                value={countTotal}
                onChange={this.handleInputChange('countTotal')}
                type="number"
                InputLabelProps={{
                  shrink: true
                }}
                InputProps={{inputProps : { max: 15, min:1}}}
                margin="normal"
              /> 
            </MenuItem>
            <MenuItem>
              <TextField
                id="maxValue"
                label="Maximum number value"
                value={maxValue}
                onChange={this.handleInputChange('maxValue')}
                type="number"
            
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
                InputProps={{inputProps : { min:0}}}
              />            
            </MenuItem>
            <MenuItem>
              <TextField
                id="minValue"
                label="Minimum number value"
                value={minValue}
                onChange={this.handleInputChange('minValue')}
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
                InputProps={{inputProps : { min:1, max:{minMax}}}}
              />             
            </MenuItem>
            <MenuItem>
              <Button size="small" variant="contained" color="primary" onClick={this.populateBranch()}>
                Go!
              </Button>            
            </MenuItem>

            
            </MenuList>            
          </form>

        </Popover>
      </ExpansionPanel>
      </div>
    );
  }
  
  
};

Branch.propTypes = {
  user: PropTypes.object,
  branch: PropTypes.object,
  populateBranch: PropTypes.func,
  openBranchEditor: PropTypes.func
};

export default Branch;