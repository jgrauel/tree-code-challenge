import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import configureStore from "reducers/configureStore";
import {loadState} from 'api/localStorage';
import App from "components/App";
const localStore = loadState();

const store = configureStore(localStore);// could pass initial state when rendering SS
//
function renderApp(ConnApp){
  const app = (
    <Provider store={store}>
      <ConnApp />
    </Provider>
  )
  ReactDOM.render(app, document.getElementById("tree-app"));
}

// Execute the first render of our app.
renderApp(App);

